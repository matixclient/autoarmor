package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.settings.type.ClientSettingBoolean;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.CPacketClientStatus;
import net.minecraft.network.play.client.CPacketCloseWindow;

import java.util.ArrayList;

/**
 * Created by Lars on 06.08.2016.
 */
public class ModuleAutoArmor extends Module {
  public ModuleAutoArmor() {
    super("AutoArmor", ModuleCategory.COMBAT);

    this.setVersion("1.0.1");
    this.setBuildVersion(16301);
    this.setDescription("Automatically puts on armor");

    this.getModuleSettings().put("preferEnc", new ClientSettingBoolean("Prefer Enchanted", true));
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent tickEvent) {
    if (Wrapper.getMinecraft().currentScreen instanceof GuiContainer) {
      return;
    }

    for (EntityEquipmentSlot equipmentSlot : EntityEquipmentSlot.values()) {
      if (equipmentSlot.getSlotType() == EntityEquipmentSlot.Type.ARMOR) {
        ItemStack itemStack = Wrapper.getPlayer().getItemStackFromSlot(equipmentSlot);

        if (itemStack == null || itemStack.getItem() == Item.getItemById(0)) {
          this.findArmor(equipmentSlot);
        }
      }
    }
  }

  private void findArmor(EntityEquipmentSlot equipmentSlot) {
    ArrayList<Slot> possibleItemStacks = new ArrayList<>();
    Wrapper.getPlayer().inventoryContainer.inventorySlots.forEach(inventorySlot -> {
      ItemStack itemStackInSlot = inventorySlot.getStack();

      if (itemStackInSlot != null && itemStackInSlot.getItem() instanceof ItemArmor) {
        ItemArmor itemArmor = (ItemArmor) itemStackInSlot.getItem();

        if (itemArmor.getEquipmentSlot() == equipmentSlot) {
          possibleItemStacks.add(inventorySlot);
        }
      }
    });

    possibleItemStacks.sort((inventorySlot, otherInventorySlot) -> {
      ItemStack itemStack = inventorySlot.getStack();
      ItemStack otherItemStack = otherInventorySlot.getStack();

      ItemArmor itemArmor = (ItemArmor) itemStack.getItem();
      ItemArmor otherItemArmor = (ItemArmor) otherItemStack.getItem();
      int armorMaterial = itemArmor.getArmorMaterial().ordinal();
      int otherArmorMaterial = otherItemArmor.getArmorMaterial().ordinal();

      if (armorMaterial > otherArmorMaterial) {
        return -1;
      } else if (otherArmorMaterial > armorMaterial) {
        return 1;
      } else {
        int protectionLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(0), itemStack);
        int otherProtectionLevel = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(0), otherItemStack);

        if (this.getValueOrDefault("preferEnc", Boolean.class, true)) {
          return protectionLevel > otherProtectionLevel ? -1 : otherProtectionLevel > protectionLevel ? 1 : 0;
        } else {
          return protectionLevel > otherProtectionLevel ? 1 : otherProtectionLevel > protectionLevel ? -1 : 0;
        }
      }
    });

    if (possibleItemStacks.size() > 0) {
      this.replaceArmor(possibleItemStacks.get(0));
    }
  }

  private void replaceArmor(Slot inventorySlot) {
    final int slotID = inventorySlot.slotNumber;
    final int windowID = Wrapper.getPlayer().inventoryContainer.windowId;

    Wrapper.getSendQueue().sendPacket(new CPacketClientStatus(CPacketClientStatus.State.OPEN_INVENTORY_ACHIEVEMENT));
    try {
      Wrapper.getMinecraft().playerController.windowClick(windowID, slotID, 0, ClickType.QUICK_MOVE, Wrapper.getPlayer());
    } catch (NullPointerException e) {
      e.printStackTrace();
    }
    Wrapper.getSendQueue().sendPacket(new CPacketCloseWindow(windowID));
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
